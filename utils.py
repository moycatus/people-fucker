import pymysql
from config import *


def get_db():
    return pymysql.connect(
        host=DB_HOST, port=DB_PORT,
        user=DB_USERNAME, password=DB_PASSWORD,
        database=DB_NAME, charset="utf8mb4"
    )


def init_article_table(db):
    with db.cursor() as cursor:
        cursor.execute("SET sql_notes = 0;")
        cursor.execute("""
        create table if not exists articles
        (
            id int auto_increment primary key,
            title varchar(512) not null,
            content longtext not null,
            published_at date not null,
            index articles_published_at_index (published_at)
        ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        """)
        cursor.execute("SET sql_notes = 0;")
        cursor.close()
    db.commit()


def init_article_keyword_table(db):
    with db.cursor() as cursor:
        cursor.execute("SET sql_notes = 0;")
        cursor.execute("""
        create table if not exists article_keywords
        (
            article_id int not null primary key,
            keywords json not null,
            weight int not null
        ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        """)
        cursor.execute("SET sql_notes = 0;")
        cursor.close()
    db.commit()


def init_day_keyword_table(db):
    with db.cursor() as cursor:
        cursor.execute("SET sql_notes = 0;")
        cursor.execute("""
        create table if not exists day_keywords
        (
            day date not null,
            keywords json null,
            constraint day_keywords_pk
                primary key (day)
        ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        """)
        cursor.execute("SET sql_notes = 0;")
        cursor.close()
    db.commit()


def init_month_keyword_table(db):
    with db.cursor() as cursor:
        cursor.execute("SET sql_notes = 0;")
        cursor.execute("""
        create table if not exists month_keywords
        (
            month date not null,
            keywords json null,
            constraint month_keywords_pk
                primary key (month)
        ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        """)
        cursor.execute("SET sql_notes = 0;")
        cursor.close()
    db.commit()


STOPWORDS = ["没有", "不准", "不能", "有关", "举行", "参加", "进行", "大家", "专栏"]


def filter_words(keywords):
    for word in STOPWORDS:
        if word in keywords:
            del keywords[word]
