import json
from collections import Counter
from collections import defaultdict
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request
from flask import url_for
from queue import PriorityQueue

from utils import get_db
from utils import filter_words

app = Flask(__name__)
db = get_db()


def get_day_keywords(date):
    # get keywords
    sql = "SELECT keywords FROM day_keywords WHERE day=%s"
    with db.cursor() as cursor:
        cursor.execute(sql, date)
        if not cursor.rowcount:
            return None
        keywords = json.loads(cursor.fetchone()[0])
    if not keywords:
        return keywords, None
    filter_words(keywords)
    # get trend for each keyword
    q = PriorityQueue()
    [q.put((-v, k), False) for k, v in keywords.items()]
    parsed_date = datetime.strptime(date, "%Y-%m-%d")
    start_date = datetime(year=parsed_date.year, month=parsed_date.month, day=1)
    end_date = start_date + relativedelta(months=1)
    keys = [q.get()[1] for _ in range(12)]
    sql = "SELECT day" + ", json_extract(keywords, %s)" * len(
        keys) + " from day_keywords where day >= %s and day < %s order by day"
    with db.cursor() as cursor:
        cursor.execute(sql,
                       (*(f"$.\"{k}\"" for k in keys), start_date.strftime("%Y-%m-%d"), end_date.strftime("%Y-%m-%d")))
        raw_trends = cursor.fetchall()
    trends = defaultdict(dict)
    for t in raw_trends:
        for i, k in enumerate(keys):
            trends[k][t[0].strftime("%Y-%m-%d")] = t[i + 1]
    return keywords, trends


def get_month_keywords(date):
    sql = "SELECT keywords FROM month_keywords WHERE month=%s"
    with db.cursor() as cursor:
        cursor.execute(sql, date + "-01")
        if not cursor.rowcount:
            return None
        keywords = json.loads(cursor.fetchone()[0])
    if not keywords:
        return keywords, None
    filter_words(keywords)
    # get trend for each keyword
    q = PriorityQueue()
    [q.put((-v, k), False) for k, v in keywords.items() if not q.full()]
    parsed_date = datetime.strptime(date, "%Y-%m")
    start_date = datetime(year=parsed_date.year, month=1, day=1)
    end_date = start_date + relativedelta(years=1)
    keys = [q.get()[1] for _ in range(16)]
    sql = "SELECT month" + ", json_extract(keywords, %s)" * len(
        keys) + " from month_keywords where month >= %s and month < %s order by month"
    with db.cursor() as cursor:
        cursor.execute(sql,
                       (*(f"$.\"{k}\"" for k in keys), start_date.strftime("%Y-%m-%d"), end_date.strftime("%Y-%m-%d")))
        raw_trends = cursor.fetchall()
    trends = defaultdict(dict)
    for t in raw_trends:
        for i, k in enumerate(keys):
            trends[k][t[0].strftime("%Y-%m-%d")[:-3]] = t[i + 1]
    return keywords, trends


@app.route("/keywords/<int:year>")
def get_year_keywords_api(year):
    sql = "SELECT keywords FROM month_keywords WHERE month >= %s and month < %s"
    with db.cursor() as cursor:
        cursor.execute(sql, (f"{year}-01-01", f"{year + 1}-01-01"))
        results = cursor.fetchall()
    if not results:
        return jsonify(None)
    keywords = sum((Counter(json.loads(r[0])) for r in results), Counter())
    if not keywords:
        return jsonify(None)
    filter_words(keywords)
    return jsonify(keywords) if keywords else jsonify(None)


@app.route("/keywords/<int:year>/<int:month>")
def get_month_keywords_api(year, month):
    sql = "SELECT keywords FROM month_keywords WHERE month = %s"
    with db.cursor() as cursor:
        cursor.execute(sql, (f"{year}-{month}-01",))
        result = cursor.fetchone()
    if not result:
        return jsonify(None)
    keywords = json.loads(result[0])
    if not keywords:
        return jsonify(None)
    filter_words(keywords)
    q = PriorityQueue()
    [q.put((-v, k)) for k, v in keywords.items() if not q.full()]
    ks = [q.get()[1] for _ in range(12)]
    return jsonify([(k, keywords[k]) for k in ks])


@app.route("/view")
def view():
    date = request.args.get("date")
    elements = date.split("-")
    # parse date
    if len(elements) == 3:
        date_format = "%Y-%m-%d"
        date_offset = timedelta(days=1)
        keywords, trends = get_day_keywords(date)
    elif len(elements) == 2:
        date_format = "%Y-%m"
        date_offset = relativedelta(months=1)
        keywords, trends = get_month_keywords(date)
    else:
        return render_template("error.html", error="Bad Request"), 400
    # get info
    parsed_date = datetime.strptime(date, date_format)
    previous_url = url_for("view") + "?date=" + (parsed_date - date_offset).strftime(date_format)
    next_url = url_for("view") + "?date=" + (parsed_date + date_offset).strftime(date_format)
    if keywords is None:
        return render_template("error.html", error="No Data", date=date, previous_url=previous_url,
                               next_url=next_url), 404
    # trans words
    keywords = json.dumps([(k, v) for k, v in keywords.items()] * 4)
    trends = json.dumps(
        [{"key": k, "values": [{"x": d, "y": float(w) if w else 0} for d, w in t.items()]} for k, t in trends.items()])
    return render_template("view.html", date=date, keywords=keywords, trends=trends, previous_url=previous_url,
                           next_url=next_url)


@app.route("/animation")
def animation():
    return render_template("animation.html")


@app.route("/")
def home():
    return render_template("index.html")


class CheckDatabaseMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        db.ping(reconnect=True)
        return self.app(environ, start_response)


if __name__ == "__main__":
    app.wsgi_app = CheckDatabaseMiddleware(app.wsgi_app)
    app.run(debug=True)
