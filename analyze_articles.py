import argparse
import json
import pymysql
from collections import namedtuple
from datetime import datetime
from datetime import timedelta
from jieba.analyse import textrank
from multiprocessing import Process
from multiprocessing import Queue

from utils import get_db
from utils import init_article_keyword_table
from utils import init_article_table

DATE_FORMAT = "%Y-%m-%d"

Keywords = namedtuple("Keywords", ("article_id", "keywords", "weight"))

db = get_db()
date_queue = Queue()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--start-date", type=str, default="1946-05-15", help="date to start analyzing")
    parser.add_argument("--end-date", type=str, default="2019-12-01", help="date to finish analyzing")
    parser.add_argument("-w", "--worker", type=int, default=4, help="number of workers to analyze")
    return parser.parse_args()


def get_todo_articles(date):
    sql = "SELECT id, title, content FROM articles WHERE published_at = %s AND " \
          "NOT EXISTS(SELECT 1 FROM article_keywords WHERE article_id = id)"
    with db.cursor(pymysql.cursors.DictCursor) as cursor:
        cursor.execute(sql, (date,))
        articles = cursor.fetchall()
    return articles


def analyze_text(text):
    keywords = textrank(text, withWeight=True)
    return {k: w for k, w in keywords}


def save(results):
    sql = "INSERT INTO article_keywords (article_id, keywords, weight) VALUES (%s, %s, %s)"
    with db.cursor() as cursor:
        count = cursor.executemany(sql, ((r.article_id, json.dumps(r.keywords), r.weight) for r in results))
    db.commit()
    print(f"analyzed {count} article(s)")


def work():
    global db
    db = get_db()
    while True:
        date = date_queue.get()
        if date is None:
            return
        # get this day's all articles
        articles = get_todo_articles(date)
        if articles:
            print(f"{len(articles)} article(s) need analyzing on {date}")
            results = []
            # analyze this day's all articles
            for article in articles:
                text = article["title"] + " " + article["content"]
                keywords = analyze_text(text)
                result = Keywords(article["id"], keywords, len(text))
                results.append(result)
            # save in db
            save(results)


def main(args):
    # parse args
    start_date = datetime.strptime(args.start_date, DATE_FORMAT)
    end_date = datetime.strptime(args.end_date, DATE_FORMAT)
    current_date = start_date
    print("analyzer started.")
    print(f"task: {start_date.strftime(DATE_FORMAT)} ~ {end_date.strftime(DATE_FORMAT)}")
    # add dates to queue
    while current_date <= end_date:
        date = current_date.strftime(DATE_FORMAT)
        date_queue.put(date)
        current_date += timedelta(days=1)
    [date_queue.put(None) for _ in range(args.worker)]
    # start workers
    workers = []
    for _ in range(args.worker):
        worker = Process(target=work)
        worker.start()
        workers.append(worker)
        date_queue.put(None)
    # wait for finishing
    for worker in workers:
        worker.join()


if __name__ == "__main__":
    init_article_table(db)
    init_article_keyword_table(db)
    main(parse_args())
