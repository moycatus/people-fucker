import argparse
import re
import requests
import time
import threading
import threadpool
from bs4 import BeautifulSoup
from collections import namedtuple
from datetime import datetime
from datetime import timedelta
from queue import Queue

from utils import get_db
from utils import init_article_table

DAY_URL = "http://navi.cnki.net/knavi/NPaperDetail/GetArticleDataXsltByDate"
ARTICLE_URL = "http://wap.cnki.net/touch/web/Newspaper/Article/{}.html"
SQL = "INSERT INTO articles (title, content, published_at) VALUES (%s, %s, %s);"
DATE_FORMAT = "%Y-%m-%d"
PAGE_ID_PATTERN = re.compile(r";fileName=(.*?)&amp")
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/79.0.3945.88 Safari/537.36",
    "X-Requested-With": "XMLHttpRequest",
    "Origin": "http://navi.cnki.net",
    "Referer": "http://navi.cnki.net/knavi/NPaperDetail?pcode=CCND&bzpym=RMRB",
    "DNT": "1",
    "Cookie": "Ecp_notFirstLogin=ECMWyC; LID=WEEvREcwSlJHSldRa1FhdXNXaEhoRFVraFQzc2ZkNE5qYWo2bTJjTUdKOD0%3D"
              "%249A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!; Ecp_session=1; "
              "Ecp_ClientId=5191213135301727524; cnkiUserKey=927790e8-f22f-9f06-ac25-7824325afc20; "
              "ASP.NET_SessionId=l23jetagxvq4rq33rojobped; SID_navi=1201610; "
              "UM_distinctid=16f56494907ac8-020f1ff28dec3e-1d306b5a-1aeaa0-16f564949087ee; "
              "UserAMID=wap_9a7055c2-a534-41a3-bd00-824b4027a23d; "
              "LID=WEEvREcwSlJHSldRa1FhdXNXaEhoRFVsZkNhbGJjbjRxaEMzQURoVUhSbz0"
              "=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!; Ecp_LoginStuts={\"IsAutoLogin\":false,"
              "\"UserName\":\"xn0116\",\"ShowName\":\"%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E5%A4%A7%E5%AD%A6\","
              "\"UserType\":\"bk\",\"BUserName\":\"\",\"BShowName\":\"\",\"BUserType\":\"\",\"r\":\"ECMWyC\"}; "
              "c_m_LinID=LinID=WEEvREcwSlJHSldRa1FhdXNXaEhoRFVsZkNhbGJjbjRxaEMzQURoVUhSbz0"
              "=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&ot=12/31/2019 12:12:12; "
              "c_m_expire=2019-12-31 12:12:12; _pk_ses=* "
}

Article = namedtuple("Article", ("title", "content"))

db = get_db()
session = requests.session()
buffer = Queue()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--start-date", type=str, default="2004-01-01", help="date to start crawling")
    parser.add_argument("--end-date", type=str, default="2019-12-01", help="date to finish crawling")
    parser.add_argument("-t", "--thread", type=int, default=8, help="crawler thread number")
    return parser.parse_args()


def get_day(date):
    while True:
        print(f"getting day {date}")
        time.sleep(2)
        try:
            day_page = session.post(DAY_URL, headers=HEADERS, data={
                "py": "RMRB",
                "pcode": "CCND",
                "pageIndex": "1",
                "pageSize": "200",
                "date": date
            }, timeout=5)
        except Exception as e:
            print(f"error getting page ids on {date}: {str(e)}")
            continue
        page_ids = list(set(PAGE_ID_PATTERN.findall(day_page.content.decode())))
        if page_ids or not day_page.content.decode().startswith("<script>"):
            break
    return page_ids


def get_article(page_id):
    url = ARTICLE_URL.format(page_id)
    while True:
        print(f"getting page {page_id}")
        time.sleep(1)
        try:
            res = session.get(url, headers=HEADERS, timeout=3)
            soup = BeautifulSoup(res.content, "html.parser")
            title = soup.find("div", class_="c-card__title2").get_text()
            content = soup.find("div", class_="c-card__aritcle").get_text()
            if not len(title) or not len(content):
                raise Exception("title or content not existing: " + page_id)
        except Exception as e:
            print(f"error getting article {page_id}: {str(e)}")
            continue
        else:
            break
    return Article(title.strip(), content.strip())


def save():
    while True:
        date, articles = buffer.get()
        if date is None:
            return
        with db.cursor() as cursor:
            count = cursor.executemany(SQL, ((a.title, a.content, date) for a in articles))
        print(f"saved {count} article(s) on {date}.")
        db.commit()


def crawl(date):
    articles = []
    print(f"crawling {date}...")
    page_ids = get_day(date)
    for page_id in page_ids:
        new_article = get_article(page_id)
        articles.append(new_article)
    if len(articles) > 0:
        buffer.put((date, articles))


def main(args):
    # parse args
    pool = threadpool.ThreadPool(args.thread)
    start_date = datetime.strptime(args.start_date, DATE_FORMAT)
    end_date = datetime.strptime(args.end_date, DATE_FORMAT)
    current_date = start_date
    print("crawler started.")
    print(f"task: {start_date.strftime(DATE_FORMAT)} ~ {end_date.strftime(DATE_FORMAT)}")
    # start saver thread
    saver = threading.Thread(target=save)
    saver.start()
    dates = []
    # add dates to queue
    while current_date <= end_date:
        date = current_date.strftime(DATE_FORMAT)
        dates.append(((date,), None))
        current_date += timedelta(days=1)
    reqs = threadpool.makeRequests(crawl, dates)
    # use thread pool to crawl
    [pool.putRequest(req) for req in reqs]
    # wait for finishing
    pool.wait()
    buffer.put((None, None))
    saver.join()


if __name__ == "__main__":
    init_article_table(db)
    main(parse_args())
