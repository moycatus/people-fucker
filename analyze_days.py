import argparse
import json
import pymysql
from collections import namedtuple
from datetime import datetime
from datetime import timedelta
from jieba.analyse import textrank
from multiprocessing import Process
from multiprocessing import Queue

from utils import get_db
from utils import init_day_keyword_table
from utils import init_article_table

DATE_FORMAT = "%Y-%m-%d"

Keywords = namedtuple("Keywords", ("day", "keywords"))

db = get_db()
date_queue = Queue()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--start-date", type=str, default="1946-05-15", help="date to start analyzing")
    parser.add_argument("--end-date", type=str, default="2019-12-01", help="date to finish analyzing")
    parser.add_argument("-w", "--worker", type=int, default=4, help="number of workers to analyze")
    return parser.parse_args()


def get_result(date):
    sql = "SELECT 1 FROM day_keywords WHERE day = %s"
    with db.cursor() as cursor:
        cursor.execute(sql, (date,))
        return cursor.rowcount != 0


def get_todo_articles(date):
    sql = "SELECT title, content FROM articles WHERE published_at = %s"
    with db.cursor(pymysql.cursors.DictCursor) as cursor:
        cursor.execute(sql, (date,))
        articles = cursor.fetchall()
    return articles


def analyze_text(text):
    keywords = textrank(text, withWeight=True, topK=20)
    return {k: w for k, w in keywords}


def save(result):
    sql = "INSERT INTO day_keywords (day, keywords) VALUES (%s, %s)"
    with db.cursor() as cursor:
        cursor.execute(sql, (result.day, json.dumps(result.keywords)))
    db.commit()


def work():
    global db
    db = get_db()
    while True:
        date = date_queue.get()
        if date is None:
            return
        if get_result(date):
            continue
        # get this day's all articles
        articles = get_todo_articles(date)
        if articles:
            # analyze all articles on this day
            text = " ".join(a['title'] + " " + a['content'] for a in articles)
            print(f"{len(articles)} article(s) are on {date}, length {len(text)}")
            keywords = analyze_text(text)
            result = Keywords(date, keywords)
            # save in db
            save(result)
            print(f"analyzed {date}")
        else:
            save(Keywords(date, None))


def main(args):
    # parse args
    start_date = datetime.strptime(args.start_date, DATE_FORMAT)
    end_date = datetime.strptime(args.end_date, DATE_FORMAT)
    current_date = start_date
    print("analyzer started.")
    print(f"task: {start_date.strftime(DATE_FORMAT)} ~ {end_date.strftime(DATE_FORMAT)}")
    # add dates to queue
    while current_date <= end_date:
        date = current_date.strftime(DATE_FORMAT)
        date_queue.put(date)
        current_date += timedelta(days=1)
    [date_queue.put(None) for _ in range(args.worker)]
    # start workers
    workers = []
    for _ in range(args.worker):
        worker = Process(target=work)
        worker.start()
        workers.append(worker)
        date_queue.put(None)
    # wait for finishing
    for worker in workers:
        worker.join()


if __name__ == "__main__":
    init_article_table(db)
    init_day_keyword_table(db)
    main(parse_args())
