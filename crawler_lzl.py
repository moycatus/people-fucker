import argparse
import requests
import threading
import threadpool
from bs4 import BeautifulSoup
from collections import namedtuple
from datetime import datetime
from datetime import timedelta
from queue import Queue

from utils import get_db
from utils import init_article_table

BASE_URL = "http://www.laoziliao.net/rmrb/"
SQL = "INSERT INTO articles (title, content, published_at) VALUES (%s, %s, %s);"
DATE_FORMAT = "%Y-%m-%d"

Article = namedtuple("Article", ("title", "content"))

db = get_db()
session = requests.session()
buffer = Queue()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--start-date", type=str, default="1946-05-15", help="date to start crawling")
    parser.add_argument("--end-date", type=str, default="2003-12-31", help="date to finish crawling")
    parser.add_argument("-t", "--thread", type=int, default=16, help="crawler thread number")
    return parser.parse_args()


def get_day(date):
    url = BASE_URL + date
    day_page = session.get(url)
    soup = BeautifulSoup(day_page.content, "html.parser")
    links = soup.find("div", class_="main").find_all("li")
    return list(set(link.a['href'].split("#")[0] for link in links))


def get_articles(url):
    res = session.get(url)
    soup = BeautifulSoup(res.content, "html.parser")
    body = soup.find("div", class_="main").div
    titles = body.find_all("h2")
    contents = body.find_all("div")
    if len(titles) != len(contents):
        raise Exception("title & content count not match!")
    return [Article(t.get_text().strip(), c.get_text().strip()) for t, c in zip(titles, contents)]


def save():
    while True:
        date, articles = buffer.get()
        if date is None:
            return
        with db.cursor() as cursor:
            count = cursor.executemany(SQL, ((a.title, a.content, date) for a in articles))
        print(f"saved {count} article(s) on {date}.")
        db.commit()


def crawl(date):
    articles = []
    print(f"crawling {date}...")
    urls = get_day(date)
    for url in urls:
        new_articles = get_articles(url)
        articles.extend(new_articles)
    if len(articles) > 0:
        buffer.put((date, articles))


def main(args):
    # parse args
    pool = threadpool.ThreadPool(args.thread)
    start_date = datetime.strptime(args.start_date, DATE_FORMAT)
    end_date = datetime.strptime(args.end_date, DATE_FORMAT)
    current_date = start_date
    print("crawler started.")
    print(f"task: {start_date.strftime(DATE_FORMAT)} ~ {end_date.strftime(DATE_FORMAT)}")
    # start saver thread
    saver = threading.Thread(target=save)
    saver.start()
    dates = []
    # add dates to queue
    while current_date <= end_date:
        date = current_date.strftime(DATE_FORMAT)
        dates.append(((date, ), None))
        current_date += timedelta(days=1)
    reqs = threadpool.makeRequests(crawl, dates)
    # use thread pool to crawl
    [pool.putRequest(req) for req in reqs]
    # wait for finishing
    pool.wait()
    buffer.put((None, None))
    saver.join()


if __name__ == "__main__":
    init_article_table(db)
    main(parse_args())
